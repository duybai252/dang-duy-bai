package com.vnpt.examapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.vnpt.examapplication.R;
import com.vnpt.examapplication.constants.ConstText;
import com.vnpt.examapplication.model.request.LoginRequest;
import com.vnpt.examapplication.model.response.LoginResponse;
import com.vnpt.examapplication.network.ApiManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.tvLoginTitle)
    TextView tvLoginTitle;

    @NotEmpty
    @BindView(R.id.edUsername)
    EditText edUsername;

    @Password(min = 8)
    @BindView(R.id.edPassword)
    EditText edPassword;
    @BindView(R.id.ibShowPass)
    ImageButton ibShowPass;
    @BindView(R.id.btLogin)
    Button btLogin;
    @BindView(R.id.tvForgetPassword)
    TextView tvForgetPassword;
    @BindView(R.id.btnFingerPrint)
    Button btnFingerPrint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ibShowPass, R.id.btLogin, R.id.tvForgetPassword, R.id.btnFingerPrint})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibShowPass:
                break;
            case R.id.btLogin:
                login();
                break;
            case R.id.tvForgetPassword:
                break;
            case R.id.btnFingerPrint:
                break;
        }
    }

    public void goMain() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("Username", edUsername.getText().toString());
        startActivity(intent);
    }

    private void login() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiManager.SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiManager service = retrofit.create(ApiManager.class);

        service.login(new LoginRequest(edUsername.getText().toString(), edPassword.getText().toString(), ConstText.MAMAY, ConstText.HEDIEUHANH, ConstText.FIREBASE_TOKEN)).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.body() == null) {
                    Toast.makeText(LoginActivity.this, "Không bắt được dữ liệu!!!", Toast.LENGTH_LONG);
                } else {
                    LoginResponse model = response.body();
                    if (model.getErrorCode() == 0) {
                        goMain();
                    } else {
                        Toast.makeText(LoginActivity.this, model.getErrorDesc(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("TAG", "Error: " + t);
            }
        });
    }
}