package com.vnpt.examapplication.model;

import java.util.List;

public abstract class Respone {

    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("DanhSachDonVi")
    private List<DanhSachDonVi> DanhSachDonVi;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("UserInfo")
    private UserInfo UserInfo;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("Token")
    private String Token;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("ErrorDesc")
    private String ErrorDesc;
    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("ErrorCode")
    private int ErrorCode;

    public static class DanhSachDonVi {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("DuongDanAnh")
        private String DuongDanAnh;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("DiaChi")
        private String DiaChi;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Ten")
        private String Ten;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Id")
        private int Id;
    }

    public static class UserInfo {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("SoDienThoai")
        private String SoDienThoai;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Email")
        private String Email;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("HoVaTen")
        private String HoVaTen;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("TenDangNhap")
        private String TenDangNhap;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Id")
        private int Id;
    }
}
