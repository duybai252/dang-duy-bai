package com.vnpt.examapplication.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DanhSachDonVi {
    @Expose
    @SerializedName("DuongDanAnh")
    private String DuongDanAnh;
    @Expose
    @SerializedName("DiaChi")
    private String DiaChi;
    @Expose
    @SerializedName("Ten")
    private String Ten;
    @Expose
    @SerializedName("Id")
    private int Id;

    public String getDuongDanAnh() {
        return DuongDanAnh;
    }

    public void setDuongDanAnh(String DuongDanAnh) {
        this.DuongDanAnh = DuongDanAnh;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
}
